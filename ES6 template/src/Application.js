export default class Application {
  constructor(appContainer, btnAdd, firstName, lastName, phoneNumber) {
    this.people = [];
    this.appContainer = appContainer;
    this.btnAdd = btnAdd;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
  }

  addPerson() {
    let firstNameValue = document.getElementById(this.firstName).value;
    let lastNameValue = document.getElementById(this.lastName).value;
    let phoneNumberValue = document.getElementById(this.phoneNumber).value;
    var id = 0;
    if (this.people.length > 0) {
      id = this.people[this.people.length - 1].id + 1;
    }
    this.people.push({
      id: id,
      firstName: firstNameValue,
      lastName: lastNameValue,
      phoneNumber: phoneNumberValue
    });

    this.render();
  }

  removePerson(sender) {
    var id = sender.target.dataset['id'];
    for (let index = 0; index < this.people.length; index++) {
      const person = this.people[index];
      if (person.id == id) {
        this.people.splice(index, 1);
        break;
      }
    }
    this.render();
  }

  editPerson(sender) {
    console.log('hello');
  }

  render() {
    var container = document.getElementById(this.appContainer);

    const tableRow = document.querySelector('.tableRow');
    const rowHover = () => {
      document.querySelector('.tableRow').addEventListener('mouseover', () => {
        console.log('hover dziala');
        tableRow.style.backgroundColor = 'blue';
      });
    };
    var html = `<table class="table">
        <thead><tr><th>ID</th><th>First Name</th><th>Last Name</th><th>Phone number</th><th></th></tr></thead
        <tbody>`;
    for (var index = 0; index < this.people.length; index++) {
      const person = this.people[index];
      html += `<tr class="tableRow" onmouseover="${rowHover}">
          <td>
          ${person.id}
          </td>
          <td>
          ${person.firstName} 
          </td>
          <td>
          ${person.lastName} 
          </td>
          <td>
          ${person.phoneNumber}
          </td>
          <td><button class="btn-remove" data-id="${
            person.id
          }">Remove</button></td>
          <td><button class="btn-edit" data-id="${person.id}">Edit</button></td>
          </tr>`;

      document.getElementById(this.firstName).value = null;
      document.getElementById(this.lastName).value = null;
      document.getElementById(this.phoneNumber).value = null;
    }
    html += '</tbody></table>';
    container.innerHTML = html;

    var buttons = document.getElementsByClassName('btn-remove');
    var btn_edit = document.querySelector('.btn-edit');

    for (let index = 0; index < buttons.length; index++) {
      const element = buttons[index];
      element.onclick = this.removePerson.bind(this);
    }
  }

  run() {
    this.render();
    document.getElementById(this.btnAdd).onclick = this.addPerson.bind(this);
  }
}
