/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Application.js":
/*!****************************!*\
  !*** ./src/Application.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Application; });\nclass Application {\n  constructor(appContainer, btnAdd, firstName, lastName, phoneNumber) {\n    this.people = [];\n    this.appContainer = appContainer;\n    this.btnAdd = btnAdd;\n    this.firstName = firstName;\n    this.lastName = lastName;\n    this.phoneNumber = phoneNumber;\n  }\n\n  addPerson() {\n    let firstNameValue = document.getElementById(this.firstName).value;\n    let lastNameValue = document.getElementById(this.lastName).value;\n    let phoneNumberValue = document.getElementById(this.phoneNumber).value;\n    var id = 0;\n\n    if (this.people.length > 0) {\n      id = this.people[this.people.length - 1].id + 1;\n    }\n\n    this.people.push({\n      id: id,\n      firstName: firstNameValue,\n      lastName: lastNameValue,\n      phoneNumber: phoneNumberValue\n    });\n    this.render();\n  }\n\n  removePerson(sender) {\n    var id = sender.target.dataset['id'];\n\n    for (let index = 0; index < this.people.length; index++) {\n      const person = this.people[index];\n\n      if (person.id == id) {\n        this.people.splice(index, 1);\n        break;\n      }\n    }\n\n    this.render();\n  }\n\n  editPerson(sender) {\n    console.log('hello');\n  }\n\n  render() {\n    var container = document.getElementById(this.appContainer);\n    const tableRow = document.querySelector('.tableRow');\n\n    const rowHover = () => {\n      document.querySelector('.tableRow').addEventListener('mouseover', () => {\n        console.log('hover dziala');\n        tableRow.style.backgroundColor = 'blue';\n      });\n    };\n\n    var html = `<table class=\"table\">\n        <thead><tr><th>ID</th><th>First Name</th><th>Last Name</th><th>Phone number</th><th></th></tr></thead\n        <tbody>`;\n\n    for (var index = 0; index < this.people.length; index++) {\n      const person = this.people[index];\n      html += `<tr class=\"tableRow\" onmouseover=\"${rowHover}\">\n          <td>\n          ${person.id}\n          </td>\n          <td>\n          ${person.firstName} \n          </td>\n          <td>\n          ${person.lastName} \n          </td>\n          <td>\n          ${person.phoneNumber}\n          </td>\n          <td><button class=\"btn-remove\" data-id=\"${person.id}\">Remove</button></td>\n          <td><button class=\"btn-edit\" data-id=\"${person.id}\">Edit</button></td>\n          </tr>`;\n      document.getElementById(this.firstName).value = null;\n      document.getElementById(this.lastName).value = null;\n      document.getElementById(this.phoneNumber).value = null;\n    }\n\n    html += '</tbody></table>';\n    container.innerHTML = html;\n    var buttons = document.getElementsByClassName('btn-remove');\n    var btn_edit = document.querySelector('.btn-edit');\n\n    for (let index = 0; index < buttons.length; index++) {\n      const element = buttons[index];\n      element.onclick = this.removePerson.bind(this);\n    }\n  }\n\n  run() {\n    this.render();\n    document.getElementById(this.btnAdd).onclick = this.addPerson.bind(this);\n  }\n\n}\n\n//# sourceURL=webpack:///./src/Application.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Application__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Application */ \"./src/Application.js\");\n\nconst appContainer = 'app-container';\nconst btnAdd = 'btn-add';\nconst firstName = 'firstName';\nconst lastName = 'lastName';\nconst phoneNumber = 'phoneNumber';\nconst application = new _Application__WEBPACK_IMPORTED_MODULE_0__[\"default\"](appContainer, btnAdd, firstName, lastName, phoneNumber);\napplication.run();\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ })

/******/ });