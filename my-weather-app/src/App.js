import React, { Component } from 'react';
import Weather from './components/Weather';
import './App.css';

const API_KEY = '236f6e788ddc406663b8b1bfa4b73d0a';

class App extends Component {
  state = {
    temperature: undefined,
    city: undefined,
    country: undefined,
    humidity: undefined,
    description: undefined,
    error: undefined,
    wind: undefined,
    submited: false,
  };

  getWeather = async e => {
    e.preventDefault();
    this.setState({ submited: true });
    const city = e.target.elements.city.value;
    const postal = e.target.elements.postal.value;

    const country_code = e.target.elements.country.value;
    const lang = e.target.elements.langSelection.value;
    const unit = e.target.elements.unitsSelection.value;
    try {
      const api_call = await fetch(
        `http://api.openweathermap.org/data/2.5/weather?q=${city}&zip=${postal},${country_code}&lang=${lang}&appid=${API_KEY}&units=${unit}`
      );

      const data = await api_call.json();

      if (city || postal || country_code) {
        this.setState({
          temperature: data.main.temp,
          city: data.name,
          country: data.sys.country,
          humidity: data.main.humidity,
          description: data.weather[0].description,
          wind: data.wind.speed,
          error: '',
          submited: true,
        });
      } else {
        this.setState({
          temperature: undefined,
          city: undefined,
          country: undefined,
          humidity: undefined,
          description: undefined,
          wind: undefined,
          error: 'Bad values',
          submited: false,
        });
        console.log(this.state);
        console.log(lang);
      }

      console.log(data);
    } catch (error) {
      this.setState({
        submited: false,
      });
      alert(error);
    }

    console.log(city);
    console.log(postal);

    document.getElementById('city').value = '';
    document.getElementById('postal').value = '';
    document.getElementById('country').value = '';
  };

  render() {
    let weather;
    if (this.state.submited) {
      weather = (
        <Weather
          temperature={this.state.temperature}
          city={this.state.city}
          country={this.state.country}
          humidity={this.state.humidity}
          description={this.state.description}
          wind={this.state.wind}
          error={this.state.error}
        />
      );
    }
    return (
      <div className="App">
        <form onSubmit={this.getWeather}>
          <p>
            City name: <input type="text" name="city" id="city" />
          </p>
          <p>OR</p>
          <p>
            Postal Code: <input type="text" name="postal" id="postal" />
          </p>
          <p>
            Country: <input type="text" name="country" id="country" />
          </p>
          Choose language:{' '}
          <select name="langSelection">
            <option value="de" name="de">
              German
            </option>
            <option value="pl" name="pl">
              Polish
            </option>
            <option value="en" name="en">
              English
            </option>
          </select>
          <br />
          Units:{' '}
          <select name="unitsSelection">
            <option value="imperial">Fahrenheit</option>
            <option value="metric">Celsius</option>
          </select>
          <p>
            <button type="submit">Check weather</button>
          </p>
        </form>

        {weather}
      </div>
    );
  }
}

export default App;
