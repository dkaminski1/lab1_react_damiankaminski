import React from 'react';

export default function Weather(props) {
  return (
    <div>
      <p>Location: {props.city}</p>
      <p>Description: {props.description}</p>
      <p>Country: {props.country}</p>
      <p>Humidity: {props.humidity}</p>
      <p>Temp: {props.temperature}</p>
      <p>Wind speed: {props.wind}</p>
    </div>
  );
}
