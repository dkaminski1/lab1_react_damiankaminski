import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Todos from './components/Todos';
import Header from './components/layout/Header';
import AddTodo from './components/AddTodo';
// import uuid from 'uuid'; //generate random id
import About from './components/pages/About';

import './App.css';
import axios from 'axios';

class App extends Component {
  state = {
    todos: [
      // {
      //   id: 1,
      //   title: 'Take out the trash',
      //   completed: false
      // },
      // {
      //   id: 2,
      //   title: 'Dinner with wife',
      //   completed: true
      // },
      // {
      //   id: 3,
      //   title: 'Practice React',
      //   completed: false
      // }
    ]
  };

  componentDidMount() {
    axios
      .get('https://jsonplaceholder.typicode.com/todos?_limit=10')
      .then(res => this.setState({ todos: res.data }));
  }

  //Toggle Complete
  markComplete = id => {
    this.setState({
      todos: this.state.todos.map(el => {
        if (el.id === id) {
          el.completed = !el.completed;
        }
        return el;
      })
    });
  };

  //Delete Todo
  delTodo = id => {
    axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`).then(res =>
      this.setState({
        todos: [...this.state.todos.filter(el => el.id !== id)]
      })
    );
  };

  //Add Todo
  addTodo = title => {
    console.log(title);
    // const newTodo = {
    //   id: uuid.v4(),
    //   title: title,
    //   completed: false
    // };

    axios
      .post('https://jsonplaceholder.typicode.com/todos', {
        title: title,
        completed: false
      })
      .then(res => this.setState({ todos: [...this.state.todos, res.data] }));
  };
  render() {
    return (
      <Router>
        <div className='App'>
          <Header />
          <Route
            exact
            path='/'
            render={props => (
              <React.Fragment>
                <AddTodo addTodo={this.addTodo} />
                <Todos
                  todos={this.state.todos}
                  markComplete={this.markComplete}
                  delTodo={this.delTodo}
                />
              </React.Fragment>
            )}
          />
          <Route path='/about' component={About} />
        </div>
      </Router>
    );
  }
}

export default App;
