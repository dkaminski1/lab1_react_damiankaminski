import Application from './application.js';

const appContainer = 'app-container';
const btnAdd = 'btn-add';
const firstName = 'firstName';
const lastName = 'lastName';
const phoneNumber = 'phoneNumber';

const application = new Application(
  appContainer,
  btnAdd,
  firstName,
  lastName,
  phoneNumber
);
application.run();
